Readme file for the Poem CAPTCHA module for Drupal
---------------------------------------------

Poem CAPTCHA is an unique kind of CAPTCHA which uses verses of poems
displayed as an image with disturbing informations in the background
to provide a challenge. This module is a submodule for the CAPTCHA 
module.

Installation:
  Installation is like with all normal drupal modules:
  extract the 'poem_captcha' folder from the tar ball to the
  modules directory from your website (typically sites/all/modules)
  or into the modules/captcha directory.
  Further steps:
  - Move the poems directory to a place inaccessible from the web.
  - Enable the module
  - Configure the module to use the poem dir's new place.
  - Add more poems or fonts if necessary.

Dependencies:
  The module depend on the basic CAPTCHA module, and need GD and 
  TrueType support.
  No UTF-8 support with PHP's builtin font! If you want to use UTF-8
  characters, you should use UTF-8 enabled ttf fonts!

Conflicts/known issues:
  See the README of the CAPTCHA module

Configuration:
  The configuration page is at admin/config/people/captcha/poem_captcha
  inside the main CAPTCHA module. You can tweak the poem CAPTCHA to your 
  liking there.
