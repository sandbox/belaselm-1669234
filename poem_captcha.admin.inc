<?php

/**
 * @file
 * Functions for administration/settings interface.
 *
 */


/**
 * Configuration form for poem_captcha.
 */
function poem_captcha_settings_form() {

  $form = array();

  // Add CSS for theming of admin form.
  $form['#attached']['css'] = array(drupal_get_path('module', 'poem_captcha') . '/poem_captcha.css');
  // Use javascript for some added usability on admin form.
  $form['#attached']['js'] = array(drupal_get_path('module', 'poem_captcha') . '/poem_captcha.js');


  // First some error checking.
  $setup_status = _poem_captcha_check_setup(FALSE);
  if ($setup_status & POEM_CAPTCHA_ERROR_NO_GDLIB) {
    drupal_set_message(t(
      'The Poem CAPTCHA module can not generate images because your PHP setup does not support it (no <a href="!gdlib">GD library</a> with JPEG support).',
      array('!gdlib' => 'http://php.net/manual/en/book.image.php')
    ), 'error');
    // It is no use to continue building the rest of the settings form.
    return $form;
  }

  $form['poem_captcha_example'] = array(
    '#type' => 'fieldset',
    '#title' => t('Example'),
    '#description' => t('Presolved poem CAPTCHA example, generated with the current settings.'),
  );
  $form['poem_captcha_example']['image'] = array(
    '#type' => 'captcha',
    '#captcha_type' => 'poem_captcha/Image',
    '#captcha_admin_mode' => TRUE,
  );

  //Path settings
  $poempath = drupal_realpath(drupal_get_path('module', 'poem_captcha')) . '/poems';
  $form['poem_captcha_poem_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to poems directory'),
    '#default_value' => variable_get('poem_captcha_poem_path', $poempath),
    '#description' => t('The path to the directory in which the poems are grouped by language. You should put it to a place inaccessible from the web!'),
  );

  // Font related stuff.
  $form['poem_captcha_font_settings'] = _poem_captcha_settings_form_font_section();

    // Color and file format settings.
  $form['poem_captcha_color_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Color and image settings'),
    '#description' => t('Configuration of the background, text colors and file format of the Poem CAPTCHA.'),
  );
  $form['poem_captcha_color_settings']['poem_captcha_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color'),
    '#description' => t('Enter the hexadecimal code for the background color (e.g. #FFF or #FFCE90). When using the PNG file format with transparent background, it is recommended to set this close to the underlying background color.'),
    '#default_value' => variable_get('poem_captcha_background_color', '#ffffff'),
    '#maxlength' => 7,
    '#size' => 8,
  );
  $form['poem_captcha_color_settings']['poem_captcha_foreground_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Text color'),
    '#description' => t('Enter the hexadecimal code for the text color (e.g. #000 or #004283).'),
    '#default_value' => variable_get('poem_captcha_foreground_color', '#000000'),
    '#maxlength' => 7,
    '#size' => 8,
  );
  $form['poem_captcha_color_settings']['poem_captcha_file_format'] = array(
    '#type' => 'select',
    '#title' => t('File format'),
    '#description' => t('Select the file format for the image. JPEG usually results in smaller files, PNG allows tranparency, in this case you should select smaller disturbance values.'),
    '#default_value' => variable_get('poem_captcha_file_format', POEM_CAPTCHA_FILE_FORMAT_JPG),
    '#options' => array(
      POEM_CAPTCHA_FILE_FORMAT_JPG => t('JPEG'),
      POEM_CAPTCHA_FILE_FORMAT_PNG => t('PNG'),
      POEM_CAPTCHA_FILE_FORMAT_TRANSPARENT_PNG => t('PNG with transparent background'),
    ),
  );

  // Noise settings
  $form['poem_captcha_noise'] = array(
    '#type' => 'fieldset',
    '#title' => t('Disturbances'),
    '#description' => t('With these settings you can control the degree of obfuscation by added text and noise. Do not exaggerate the obfuscation and assure that the poem in the image is reasonably readable.'),
  );
  //Disturbance text settings
  $form['poem_captcha_noise']['poem_captcha_disturb_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Disturbance text'),
    '#default_value' => variable_get('poem_captcha_disturb_text', 'NOSPAMPLEASE'),
    '#description' => t('This text is written under the poem in a near-background color. It will be multiplied to the needed length. Spaces are not necessary :)'),
  );
  // noise
  $form['poem_captcha_noise']['poem_captcha_disturb_min'] = array(
    '#type' => 'select',
    '#title' => t('Minimum disturbance level'),
    '#options' => array(
      10 => '10',
      20 => '20',
      30 => '30',
      50 => '50',
      70 => '70',
      90 => '90',
      110 => '110',
    ),
    '#default_value' => (int) variable_get('poem_captcha_disturb_min', 10),
    '#description' => t('Minimum difference of disturbance text color from background color.'),
  );
  $form['poem_captcha_noise']['poem_captcha_disturb_max'] = array(
    '#type' => 'select',
    '#title' => t('Maximum disturbance level'),
    '#options' => array(
      20 => '20',
      30 => '30',
      40 => '40',
      60 => '60',
      80 => '80',
      100 => '100',
      120 => '120',
    ),
    //SHOULDN'T BE MORE THAN 128!
    '#default_value' => (int) variable_get('poem_captcha_disturb_max', 30),
    '#description' => t('Maximum difference of disturbance text color from background color. If you use medium background color (one of the values are close to #80) do not select too big max disturbance!'),
  );

  // Add a validation handler.
  $form['#validate'] = array('poem_captcha_settings_form_validate');

  // Make it a settings form.
  $form = system_settings_form($form);
  // But also do some custom submission handling.
  $form['#submit'][] = 'poem_captcha_settings_form_submit';

  return $form;
}


/**
 * Form elements for the font specific setting.
 *
 * This is refactored to a separate function to avoid poluting the
 * general form function poem_captcha_settings_form with some
 * specific logic.
 *
 * @return $form, the font settings specific form elements.
 */
function _poem_captcha_settings_form_font_section() {
  // First check if there is TrueType support.
  $setup_status = _poem_captcha_check_setup(FALSE);
  if ($setup_status & POEM_CAPTCHA_ERROR_NO_TTF_SUPPORT) {
    // Show a warning that there is no TrueType support
    $form['no_ttf_support'] = array(
      '#type' => 'item',
      '#title' => t('No TrueType support'),
      '#markup' => t('The Poem CAPTCHA module can not use TrueType fonts because your PHP setup does not support it. You can only use a PHP built-in bitmap font of fixed size.'),
    );

  }
  else {

    // Build a list of  all available fonts.
    $available_fonts = array();

    // List of folders to search through for TrueType fonts.
    $fonts = _poem_captcha_get_available_fonts_from_directories();
    // Cache the list of previewable fonts. All the previews are done
    // in separate requests, and we don't want to rescan the filesystem
    // every time, so we cache the result.
    variable_set('poem_captcha_fonts_preview_map_cache', $fonts);
    // Put these fonts with preview image in the list
    foreach ($fonts as $token => $font) {
      $img_src = check_url(url('admin/config/people/captcha/poem_captcha/font_preview/' . $token));
      $title = t('Font preview of @font (@file)', array('@font' => $font->name, '@file' => $font->uri));
      $available_fonts[$font->uri] = '<img src="' . $img_src . '" alt="' . $title . '" title="' . $title . '" />';
    }

    // Append the PHP built-in font at the end.
    $img_src = check_url(url('admin/config/people/captcha/poem_captcha/font_preview/BUILTIN'));
    $title = t('Preview of built-in font');
    $available_fonts['BUILTIN'] = t('PHP built-in font (no UTF-8 support!): !font_preview',
      array('!font_preview' => '<img src="' . $img_src . '" alt="' . $title . '" title="' . $title . '" />')
    );

    $default_font = variable_get('poem_captcha_font', 'BUILTIN');

    //font selection
    $form['poem_captcha_font'] = array(
      '#type' => 'radios',
      '#title' => t('Fonts'),
      '#default_value' => $default_font,
      '#options' => $available_fonts,
      '#description' => t('Select the font to use for the text in the Poem CAPTCHA. Apart from the provided defaults, you can also use your own TrueType fonts with .ttf filename extension by putting them in %fonts_library_general or %fonts_library_specific.',
        array(
          '%fonts_library_general' => 'sites/all/libraries/fonts',
          '%fonts_library_specific' => conf_path() . '/libraries/fonts',
        )
      ),
    );

    // Font size.
    $form['poem_captcha_font_size'] = array(
      '#type' => 'select',
      '#title' => t('Font size'),
      '#options' => array(
        9 => '9 pt - ' . t('small'),
        12 => '12 pt - ' . t('normal'),
        16 => '16 pt - ' . t('larger'),
        20 => '20 pt - ' . t('large'),
        24 => '24 pt - ' . t('extra large'),
      ),
      '#default_value' => (int) variable_get('poem_captcha_font_size', 30),
      '#description' => t('The font size influences the size of the image. Note that larger values make the image generation more CPU intensive.'),
    );

  }

  return $form;
}

/**
 * Helper function to get fonts from the given directories.
 *
 * @param $directories (optional) an array of directories
 *   to recursively search through, if not given, the default
 *   directories will be used.
 *
 * @return an array of fonts file objects (with fields 'name',
 *   'basename' and 'filename'), keyed on the md5 hash of the font
 *   path (to have an easy token that can be used in an url
 *   without en/decoding issues).
 */
function _poem_captcha_get_available_fonts_from_directories($directories=NULL) {
  // If no fonts directories are given: use the default.
  if ($directories === NULL) {
    $directories = array(
      drupal_get_path('module', 'poem_captcha') . '/fonts',
      'sites/all/libraries/fonts',
      conf_path() . '/libraries/fonts',
    );
  }
  // Collect the font information.
  $fonts = array();
  foreach ($directories as $directory) {
    foreach (file_scan_directory($directory, '/\.[tT][tT][fF]$/') as $filename => $font) {
      $fonts[md5($filename)] = $font;
    }
  }

  return $fonts;
}


/**
 * Validation function for poem_captcha configuration form
 */
function poem_captcha_settings_form_validate($form, &$form_state) {
  // Check poem_captcha_image_allowed_chars for spaces.
//  if (preg_match('/\s/', $form_state['values']['poem_captcha_image_allowed_chars'])) {
//    form_set_error('poem_captcha_image_allowed_chars', t('The list of characters to use should not contain spaces.'));
//  }

  if (!isset($form['poem_captcha_font_settings']['no_ttf_support'])) {
//    list($readable_fonts, $problem_fonts) = _poem_captcha_check_fonts($fonts);
//    if (count($problem_fonts) > 0) {
//      form_set_error('poem_captcha_fonts', t('The following fonts are not readable: %fonts.', array('%fonts' => implode(', ', $problem_fonts))));
//    }
  }

  // check color settings
  if (!preg_match('/^#([0-9a-fA-F]{3}){1,2}$/', $form_state['values']['poem_captcha_background_color'])) {
    form_set_error('poem_captcha_background_color', t('Background color is not a valid hexadecimal color value.'));
  }
  if (!preg_match('/^#([0-9a-fA-F]{3}){1,2}$/', $form_state['values']['poem_captcha_foreground_color'])) {
    form_set_error('poem_captcha_foreground_color', t('Text color is not a valid hexadecimal color value.'));
  }
}

/**
 * Submit function for poem_captcha configuration form.
 */
function poem_captcha_settings_form_submit($form, &$form_state) {
//  if (!isset($form['poem_captcha_font_settings']['no_ttf_support'])) {
    // Filter the poem_captcha fonts array to pick out the selected ones.
//    $fonts = array_filter($form_state['values']['poem_captcha_fonts']);
//    variable_set('poem_captcha_fonts', $fonts);
//  }
}

/**
 * Menu handler for font preview request.
 *
 */
function poem_captcha_font_preview($font_token) {

  // Get the font from the given font token.
  if ($font_token == 'BUILTIN') {
    $font = 'BUILTIN';
  }
  else {
    // Get the mapping of font tokens to font file objects.
    $fonts = variable_get('poem_captcha_fonts_preview_map_cache', array());
    if (!isset($fonts[$font_token])) {
      echo('bad token');
      exit();
    }
    // Get the font path.
    $font = $fonts[$font_token]->uri;
    // Some sanity checks if the given font is valid.
    if (!is_file($font) || !is_readable($font)) {
      echo('bad font');
      exit();
    }
  }

  // Settings of the font preview.
  $width = 120;
  $text = 'AaBbCc123';
  $font_size = 14;
  $height = 2 * $font_size;

  // Allocate image resource.
  $image = imagecreatetruecolor($width, $height);
  if (!$image) {
    exit();
  }
  // White background and black foreground.
  $background_color = imagecolorallocate($image, 255, 255, 255);
  $color = imagecolorallocate($image, 0, 0, 0);
  imagefilledrectangle($image, 0, 0, $width, $height, $background_color);

  // Draw preview text
  if ($font == 'BUILTIN') {
    imagestring($image, 5, 1, .5*$height-10, $text, $color);
  }
  else {
    imagettftext($image, $font_size, 0, 1, 1.5*$font_size, $color, realpath($font), $text);
  }

  // Set content type.
  drupal_add_http_header('Content-Type', 'image/png');
  // Dump image data to client.
  imagepng($image);
  // Release image memory.
  imagedestroy($image);

  // Close connection.
  exit();
}
