(function($) {

  Drupal.behaviors.captchaAdmin = {
    attach : function(context) {

      // Helper function to show/hide noise level widget.
      var noise_level_shower = function(speed) {
        speed = (typeof speed == 'undefined') ? 'slow' : speed;
        if ($("#edit-poem-captcha-dot-noise").is(":checked")
            || $("#edit-poem-captcha-line-noise").is(":checked")) {
          $(".form-item-poem-captcha-noise-level").show(speed);
        } else {
          $(".form-item-poem-captcha-noise-level").hide(speed);
        }
      }
      // Add onclick handler to the dot and line noise check boxes.
      $("#edit-poem-captcha-dot-noise").click(noise_level_shower);
      $("#edit-poem-captcha-line-noise").click(noise_level_shower);
      // Show or hide appropriately on page load.
      noise_level_shower(0);

      // Helper function to show/hide smooth distortion widget.
      var smooth_distortion_shower = function(speed) {
        speed = (typeof speed == 'undefined') ? 'slow' : speed;
        if ($("#edit-poem-captcha-distortion-amplitude").val() > 0) {
          $(".form-item-poem-captcha-bilinear-interpolation").show(speed);
        } else {
          $(".form-item-poem-captcha-bilinear-interpolation").hide(speed);
        }
      }
      // Add onchange handler to the distortion level select widget.
      $("#edit-poem-captcha-distortion-amplitude").change(
          smooth_distortion_shower);
      // Show or hide appropriately on page load.
      smooth_distortion_shower(0)

    }
  };

})(jQuery);
