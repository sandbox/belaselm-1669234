<?php

/**
 * @file
 * Functions for the generation of the CAPTCHA image.
 *
 * Based on ImageCaptcha
 * (http://heine.familiedeelstra.com/mycaptcha-download)
 */

/**
 * Menu callback function that generates the CAPTCHA image.
 */
function poem_captcha_image($captcha_sid=NULL) {
  // If output buffering is on: discard current content and disable further buffering
  if (ob_get_level()) {
    ob_end_clean();
  }
  
  if (!$captcha_sid) {
    exit();
  }

  // Get solution (the solution).
  $code = db_query("SELECT solution FROM {captcha_sessions} WHERE csid = :csid",
    array(':csid' => $captcha_sid)
  )->fetchField();
  // Only generate captcha if code exists in the session.
  if ($code !== FALSE) {
    //Get the code
    $code = variable_get('poem_captcha_' . $captcha_sid); variable_del('poem_captcha_' . $captcha_sid);
    // generate the image
    list($numrows, $authinfo, $poem) = _poem_captcha_get_poem($code);
    list($pno, $sno, $rno, $wno) = explode('|', $code);

    $image = @_poem_captcha_generate_image($poem);
    // check of generation was successful
    if (!$image) {
      watchdog('CAPTCHA', 'Generation of poem CAPTCHA failed. Check your poem CAPTCHA configuration and especially the used font.', array(), WATCHDOG_ERROR);
      exit();
    }
    // Send the image resource as an image file to the client.
    $file_format = variable_get('poem_captcha_file_format', POEM_CAPTCHA_FILE_FORMAT_JPG);
    if ($file_format == POEM_CAPTCHA_FILE_FORMAT_JPG) {
      drupal_add_http_header('Content-Type', 'image/jpeg');
      imagejpeg($image);
    }
    else {
      drupal_add_http_header('Content-Type', 'image/png');
      imagepng($image);
    }
    // Clean up the image resource.
    imagedestroy($image);
  }
  exit();
}


/**
 * Small helper function for parsing a hexadecimal color to a RGB tuple.
 */
function _poem_captcha_hex_to_rgb($hex) {
  // handle #RGB format
  if (strlen($hex) == 4) {
    $hex = $hex[1] . $hex[1] . $hex[2] . $hex[2] . $hex[3] . $hex[3];
  }
  $c = hexdec($hex);
  $rgb = array();
  for ($i = 16; $i >= 0; $i -= 8) {
    $rgb[] = ($c >> $i) & 0xFF;
  }
  return $rgb;
}


/**
 * Base function for generating a image CAPTCHA.
 */
function _poem_captcha_generate_image($poem) {
  //Conversion between ttf and builtin fontsizes
  $fontsizes = array('9' => '1', '12' => '2', '16' => '3', '20' => '4', '24' => '5');
  // get other settings
  $font_size = variable_get('poem_captcha_font_size');
  $disturb_min = variable_get('poem_captcha_disturb_min', '20');
  $disturb_max = variable_get('poem_captcha_disturb_max', '80');
  $disturb_text = variable_get('poem_captcha_disturb_text', 'NOSPAMPLEASE');
  list($width, $height) = _poem_captcha_image_size($poem);
  // create image resource
  $image = imagecreatetruecolor($width, $height);
  if (!$image) {
    return FALSE;
  }

  // Get the background color and paint the background.
  $background_rgb = _poem_captcha_hex_to_rgb(variable_get('poem_captcha_background_color', '#ffffff'));
  $background_color = imagecolorallocate($image, $background_rgb[0], $background_rgb[1], $background_rgb[2]);
  // Set transparency if needed.
  $file_format = variable_get('poem_captcha_file_format', POEM_CAPTCHA_FILE_FORMAT_JPG);
  if ($file_format == POEM_CAPTCHA_FILE_FORMAT_TRANSPARENT_PNG) {
    imagecolortransparent($image, $background_color);
  }
  imagefilledrectangle($image, 0, 0, $width, $height, $background_color);

  $foreground_rgb = _poem_captcha_hex_to_rgb(variable_get('poem_captcha_foreground_color', '#000000'));
  $foreground_color = imagecolorallocate($image, $foreground_rgb[0], $foreground_rgb[1], $foreground_rgb[2]);
  
  //Create a disturbance color from background
  if ($disturb_min>$disturb_max) { 
    $tmp = $disturb_min; $disturb_min = $disturb_max; $disturb_max = $tmp;
    variable_set('poem_captcha_disturb_min', $disturb_min); variable_set('poem_captcha_disturb_max', $disturb_max); 
  }
  $disturb_rgb = array();
  $disturb_color = imagecolorallocate($image, $disturb_rgb[0], $disturb_rgb[1], $disturb_rgb[2]);
  //Get font
  $font = variable_get('poem_captcha_font', 'BUILTIN');
  //Prepare disturbance text
  $dlen = strlen($disturb_text); $cnt = intval(100/$dlen);
  //If it is shorter than 100 chars we multiply it
  if ($cnt>1) { 
    $disturb_text = str_repeat($disturb_text, $cnt+1); 
  }
  $x=0; 
  if ($font =='BUILTIN') { 
    $ydiff = intval(imagefontheight($fontsizes[$font_size])*1.5); $bfont = $fontsizes[$font_size]; $y=intval($ydiff/2);
    $disty=0; } 
  else { $ydiff = intval($font_size*1.5); $y=intval($font_size*1.25); $disty=intval($font_size/2); }
  //Draw disturbance texts
  for ($i=0; $i<count($poem)+1; $i++) {
    foreach ($background_rgb as $key => $data) {
      $disturb = rand($disturb_min, $disturb_max);
      if (rand()>0.5) { 
        $disturb_rgb[$key] = $data-$disturb; } 
      else { 
        $disturb_rgb[$key] = $data+$disturb; 
      }
      if ($disturb_rgb[$key]<0) { 
        $disturb_rgb[$key]=$data+$disturb; 
      }
      if ($disturb_rgb[$key]>255) { 
        $disturb_rgb[$key]=$data-$disturb; 
      }
    }
// $disturb_rgb[0]=255; $disturb_rgb[1]=0; $disturb_rgb[2]=0;	//Test - make the disturbing text red
    $disturb_color = imagecolorallocate($image, $disturb_rgb[0], $disturb_rgb[1], $disturb_rgb[2]);
    $distx = rand(0, intval($width/2)) - $width;
    $distx = 0;
    if ($font == 'BUILTIN') {
      imagestring($image, $bfont, $distx, $disty, $disturb_text, $disturb_color);} 
    else {
      imagettftext($image, $font_size, 0, $distx, $disty, $disturb_color, drupal_realpath($font), $disturb_text);
    }
    $disty += $ydiff;
  }
  // set default text color
  $color = $foreground_color;
  // draw poem
  foreach ($poem as $data) {
    if ($font == 'BUILTIN') {
      imagestring($image, $bfont, $x, $y, $data, $color); } 
    else {
      imagettftext($image, $font_size, 0, $x, $y, $color, drupal_realpath($font), $data);
    }
    $y += $ydiff;
  }
  // For debugging purposes: draw character bounding box (only valid when rotation is disabled).
  //imagerectangle($image, 0, 0, $width-1, $height-1, $color);

  //Add some noise to the characters
  _poem_captcha_image_generator_add_dots($image, $width, $height, $font_size, $disturb_max, $foreground_rgb);

  return $image;
}

function _poem_captcha_image_generator_add_dots(&$image, $width, $height, $font_size, $disturb_max, $color_rgb) {
  $noise_rgb = array(); $linedist = intval($font_size/2);
  //draw horizontal lines through the poem text only
  for ($i=0; $i<$height; $i+=$linedist) {
    for ($j=0; $j<$width; $j++) {
      $rgb = imagecolorat($image, $j, $i);
      $r = ($rgb >> 16) & 0xFF;
      $g = ($rgb >> 8) & 0xFF;
      $b = $rgb & 0xFF;
      //pixel color is the color of poem text
      if (($r==$color_rgb[0])&&($g==$color_rgb[1])&&($b==$color_rgb[2])) {
        //Generate new disturbance color - every pixel has another color
        foreach ($color_rgb as $key => $data) {
          $disturb = rand(10, $disturb_max);
          if (rand()>0.5) { 
            $noise_rgb[$key] = $data-$disturb; } 
          else { 
            $noise_rgb[$key] = $data+$disturb; 
          }
          if ($noise_rgb[$key]<0) { 
            $noise_rgb[$key]=$data+$disturb; 
          }
          if ($noise_rgb[$key]>255) { 
            $noise_rgb[$key]=$data-$disturb; 
          }
        }
//$noise_rgb[0]=255; $noise_rgb[1]=0; $noise_rgb[2]=0;	//Test - make the noise lines red

        $noise_color = imagecolorallocate($image, $noise_rgb[0], $noise_rgb[1], $noise_rgb[2]);
        imagesetpixel($image, $j, $i, $noise_color);
      }
    }
  }
}

